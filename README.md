# ACME Groceries

## How to use

Install it and run through CLI :)

```
npm install
npm install -g
```

use acme commend before the commend
i.e.,
acme ingest foo1.txt

### Available Commends


```
acme [command] <options>

ingest ............ upload file
summery ........... summery of the sales
generate_report ... generate the Report
config ............ set the input and output directory
exit .............. quit the instance 
version ........... show package version
help .............. show help menu for a command
```
### Troubleshoot
if you are facing issue while install global. please use the following commend

```
node index.js [commend] <options>

> node index.js summery Dairy 2019 3
Dairy - Total Units: 47754 Total Gross Sales:557490.74
```
note: entry file we've to use manually instead of `acme` 