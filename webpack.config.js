const path = require('path');

module.exports = {
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'api.bundle.js'
  },
  module: {
    rules: [{ test: /\.json$/, use: 'raw-loader' }],
  },
  target: 'node'
};