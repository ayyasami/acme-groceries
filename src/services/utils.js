import chalk from 'chalk';
import fs from 'fs';
import get from 'lodash.get';
import set from 'lodash.set';

export const validatePath = (dir) => {
  if (!fs.existsSync(dir)) {
    console.error(
      chalk.redBright(
        `Directory doesn't exist.`
      )
    );
    return false;
  }
  return true;
}
export const validateFile = (dir) => {
  if (!fs.existsSync(dir)) {
    return false;
  }
  return true;
}
export const validateData = ({ headers, records, isConflictReplace }) => {
  const availableSKUs = [];
  const validRecord = []
  records.forEach(record => {
    /**
     * undefined SKU / last empty record removing
     */
    if (record.SKU && record.SKU !== "") {
      if (availableSKUs.includes(record.SKU)) {
        /**
         * Dupicate SKU: need to merge units and sales
         */
        const mergedSalesAndUnits = {}
        const existingRecordIndex = validRecord.findIndex(exRecord => exRecord.SKU === record.SKU)
        const existingRecord = validRecord[existingRecordIndex];
        headers.forEach((header) => {
          const curVal = get(record, header);
          const exVal = get(existingRecord, header);
          if (!["sku", "section"].includes(header.toLowerCase()) && curVal && exVal)
            set(mergedSalesAndUnits, header, isConflictReplace ? curVal : curVal + exVal)
          // mergedSalesAndUnits[header] = record[header] + existingRecord[header]
        })
        validRecord[existingRecordIndex] = {
          ...existingRecord,
          ...record,
          ...mergedSalesAndUnits,
        }
      } else {
        availableSKUs.push(record.SKU)
        validRecord.push(record)
      }
    }
  })

  return validRecord;
}
