import chalk from 'chalk';
import Conf from 'conf';
import fs from "fs";
import path from "path";
import { configKey } from '../action/configure';
import { validateFile } from './utils';
const config = new Conf().get(configKey);

class JsonStore {
  storePath = path.join(__dirname, "../../sample/storage.json");
  constructor() {
    if (!validateFile(this.storePath)) {
      fs.writeFileSync(this.storePath, JSON.stringify({
        data: [],
        ingestLog: [],
        createdAt: new Date().toString()
      }));
      console.info(chalk.gray(`store has initiated..`)
      );
    }
  }
  getData() {
    return this.getStore().data;
  }
  getDataSummery({ category, year, month }) {
    const monthOfYr = `${year}-${month}`;
    // console.log("_monthOfYr__", monthOfYr, category)
    return this.getStore().data.filter(record => {
      return (record.Section === category) && record[monthOfYr];
    })
      .reduce((ac, record) => {
        if (ac) {
          const acKeys = Object.keys(ac);
          const newAc = {};
          acKeys.forEach(key => newAc[key] = ac[key] + record[monthOfYr][key])
          return newAc;
        } else {
          return record[monthOfYr]
        }
      }, null);
  }
  getReport() {
    return this.getStore().data.map(({ SKU, Section: Category, ...monthYear }) => {
      return Object.keys(monthYear).map(my => {
        const [year, month] = my.split("-");
        return ({ SKU, Category, Year: +year, Month: +month, ...monthYear[my] })
      })
    }).flat()
      .filter(each => Object.keys(each).length === Object.values(each).filter(Boolean).length)
      .sort(
        (a, b) => {
          if (a.SKU === b.SKU) {
            if (a.year === b.year) {
              return a.month - b.month;
            }
            return a.year > b.year ? 1 : -1;
          }
          return a.SKU > b.SKU ? 1 : -1;
        });
  }
  getIngestLog() {
    return this.getStore().ingestLog;
  }
  updateData({ data }) {
    const store = this.getStore();
    store.data = [...data];
    fs.writeFileSync(this.storePath, JSON.stringify(store));
  }
  addIngestLog({ path }) {
    const store = this.getStore()
    store.ingestLog = [
      { path, ingestedAt: new Date().toString() },
      ...this.getIngestLog()
    ]
    fs.writeFileSync(this.storePath, JSON.stringify(store));
  }
  getStore() {
    const store = fs.readFileSync(this.storePath, 'utf8')
    return JSON.parse(store);
  }

  clear() {
    if (validateFile(this.storePath)) {
      fs.unlinkSync(this.storePath)
    }
  }
}
export { JsonStore };
