import minimist from 'minimist';
import { ingest } from './action/ingest';
import { help } from './action/help';
import { configure } from './action/configure';
import { version } from './action/version';
import { summery } from "./action/summery"
import { JsonStore } from './services/storage';
import { report } from './action/report';

export async function cli(argsArray) {
  const args = minimist(argsArray.slice(2));
  const args1 = argsArray.slice(2);
  let cmd = args._[0] || 'help';
  // console.log("_args__", cmd, args, args1, argsArray)

  if (args.version || args.v) {
    cmd = 'version';
  }

  if (args.help || args.h) {
    cmd = 'help';
  }

  switch (cmd) {
    case 'ingest':
      ingest(args);
      break;
    case 'summery':
      summery(args);
      break;
    case 'generate_report':
      report(args);
      break;
    case 'config':
      configure(args);
      break;
    case 'exit':
      const store = new JsonStore();
      store.clear();
      break;

    case 'version':
      version(args);
      break;
    case 'help':
      help(args);
      break;

    default:
      console.error(`"${cmd}" is not a valid command!`);
      break;
  }
}
