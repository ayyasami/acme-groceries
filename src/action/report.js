import chalk from 'chalk';
import Conf from 'conf';
import fs from 'fs';
import { JsonStore } from '../services/storage';
import { configKey } from './configure';
import path from 'path';

export const report = async ({ _: [cmd, fileName], ...args }) => {
  const config = new Conf().get(configKey);
  const store = new JsonStore();
  const headers = ["Year", "Month", "SKU", "Category", "GrossSales", "Units"]
  const content = store.getReport()
  const contentBuffer = content.map(con => headers.map(h => con[h]).join(","))
  // console.log("contentBuffer___", content.length)
  fs.writeFileSync(fileName, [headers.join(","), ...contentBuffer].join("\n"));
  console.log(chalk.gray(`file has exported at ${fs.realpathSync(fileName)}`))
  return;
}
