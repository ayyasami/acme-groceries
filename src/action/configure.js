import Conf from 'conf';
import { validatePath } from '../services/utils';

export const configKey = '@cm3';

export async function configure(args) {
  const config = new Conf();

  if (args.clear) {
    config.clear();
    console.log(`The configuration for acme has been deleted.
    Current value: ${config.get(configKey)}
    `);
    return;
  }

  if (args.path) {
    console.log(`The config file path is: 
    ${config.path}
    `);
    return;
  }

  let currentConfigObject = config.get(configKey);
  if (args.p || args.print) {
    console.log(currentConfigObject);
    return;
  }

  currentConfigObject = currentConfigObject || {};

  let source = args.source || args.s;
  if (!source) {
    source = currentConfigObject.source;
  }
  if (!validatePath(source)) {
    return;
  }

  let destination = args.destination || args.d;
  if (!destination) {
    destination = currentConfigObject.destination;
  }
  if (!validatePath(destination)) {
    return;
  }


  config.set(configKey, { source, destination });
}
