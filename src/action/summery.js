import chalk from 'chalk';
import Conf from 'conf';
import { configKey } from './configure';
import { JsonStore } from '../services/storage';

export const summery = async ({ _: [cmd, category, year, month, ...rest], ...args }) => {
  const config = new Conf().get(configKey);
  const store = new JsonStore();
  const resp = store.getDataSummery({ category, year, month })
  if (!resp || !resp.Units || !resp.GrossSales) {
    console.log(`${chalk.gray("No data available")}`)
    return;
  }
  console.log(chalk.yellowBright(`${category} - Total Units: ${resp.Units} Total Gross Sales:${resp.GrossSales}`))
  return;
}
