import chalk from 'chalk';

const menus = {
  main: `
${chalk.greenBright('acme [command] <options>')}

  ${chalk.blueBright('ingest')} ............ upload file
  ${chalk.blueBright('summery')} ........... summery of the sales
  ${chalk.blueBright('generate_report')} ... generate the Report
  ${chalk.blueBright('config')} ............ set the input and output files directory
  ${chalk.blueBright('exit')} .............. quit the instance 
  ${chalk.blueBright('version')} ........... show package version
  ${chalk.blueBright('help')} .............. show help menu for a command
`,

  ingest: `
${chalk.greenBright('acme ingest <options>')}

  --file-name, -f ..... [Optional] set the file name need to upload.
`,

  summery: `
${chalk.greenBright('acme summery <options>')}

  --category-name, -C ..... [Optional] set the category for summery 
  --year, -y .............. [Optional] set the year for summery
  --month, -m ............. [Optional] set the month for summery
`,

  generate_report: `
${chalk.greenBright('acme generate_report <options>')}

  --txt, -t ....... [Optional] define output format as TEXT(.txt).
  --csv, -c ....... [Optional] define output format as CSV(.csv).
`,
  config: `
${chalk.greenBright('acme config <options>')}

  --source, -s ......... [Required] set the directory of source.
  --destination, -d .... [Required] set the directory of destionation.
`,

}

export async function help(args) {
  const subCmd = args._[0] === 'help'
    ? args._[1]
    : args._[0]
  console.log(menus[subCmd] || menus.main)
}
