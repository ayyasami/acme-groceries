import chalk from 'chalk';
import Conf from 'conf';
import fs from 'fs';
import set from "lodash.set";
import path from "path";
import xlsx from 'xlsx';
import { JsonStore } from '../services/storage';
import { validateData, validateFile } from '../services/utils';
import { configKey } from './configure';

const acceptedFileFormat = [".txt", ".xlsx"]
export const ingest = async ({ _: [cmd, absoluteFile], ...args }) => {
  const config = new Conf().get(configKey);
  const store = new JsonStore();
  if (absoluteFile) {
    if (!validateFile(absoluteFile)) {
      console.log(chalk.red(`Error - File not found`))
      return;
    } else {
      const fileExt = path.extname(absoluteFile)
      if (acceptedFileFormat.includes(fileExt)) {
        // console.log("==TODO==ABS", args, path.extname(absoluteFile));
        const uploadHandler = fileExt === acceptedFileFormat[0] ? uploadTxt : uploadxlsx;
        return uploadHandler({ filePath: absoluteFile, store })
      }
      console.log(chalk.red(`${path.extname(absoluteFile)} is not a supporting format`))
      console.log(chalk.cyan("please try with", acceptedFileFormat.join(", ")))
    }
  } else {
    const relativeFileName = args.f || args['file-name'];
    const relativeFile = path.join(config.source, relativeFileName)

    if (relativeFileName && !validateFile(relativeFile)) {
      return;
    } else {
      // console.log("==TODO==REL", args, path.extname(relativeFile));
      fileUpload({ filePath: relativeFile, store })

    }
  }
}

function uploadxlsx({ filePath, store }) {
  const workbook = xlsx.readFile(filePath);
  const jsonRecords = xlsx.utils.sheet_to_json(workbook.Sheets["Sales"]);
  const headerMeta = Object.keys(jsonRecords[0]).map(rawH => rawH.split(" "))
    .map(([main, ...sub]) => ({
      actualKey: [main, ...sub].join(" "),
      aliasKey: sub.length ? `${main}.${sub.join("")}` : main
    }));
  const parsedRecords = jsonRecords.map((rec => {
    const record = {};
    headerMeta.forEach(({ actualKey, aliasKey }) =>
      set(record, aliasKey, +rec[actualKey] || rec[actualKey] || null)
    )
    return record;
  }))
  const headers = headerMeta.map(({ aliasKey }) => aliasKey);

  fileUpload({ filePath, store, parsedRecords, headers })
}
function uploadTxt({ filePath, store }) {
  const rawFile = fs.readFileSync(filePath, { encoding: 'utf8', flag: 'r' });
  const [rawHeaders, ...rows] = rawFile.replace(/\r\n/g, "\r").replace(/\n/g, "\r").split(/\r/).map(each => each.split("\t"));
  const headers = rawHeaders.map(rawH => rawH.split(" "))
    .map(([main, ...sub]) => sub.length ? `${main}.${sub.join("")}` : main);
  const parsedRecords = jsonStiching({ headers, rows });

  fileUpload({ filePath, store, parsedRecords, headers })
}
function fileUpload({ filePath, store, parsedRecords, headers }) {
  const realPath = fs.realpathSync(filePath);
  const validData = validateData({ headers, records: parsedRecords });
  const existing = store.getData();

  store.addIngestLog({ path: realPath });
  store.updateData({ data: validateData({ headers, records: [...existing, ...validData], isConflictReplace: true }) });
  console.log(chalk.yellow("Success"))
}

const jsonStiching = ({ headers, rows }) => {
  return rows.map(row => {
    const record = {};
    headers.forEach((header, index) => {
      set(record, header, +row[index] || row[index] || null)
      // record[header] = +row[index] || row[index] || null
    })
    return record;
  })
}

